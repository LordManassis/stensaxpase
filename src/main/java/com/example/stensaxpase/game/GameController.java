package com.example.stensaxpase.game;

import com.example.stensaxpase.tokens.Token;
import com.example.stensaxpase.tokens.TokenService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@AllArgsConstructor
public class GameController {

    GameService gameService;
    TokenService tokenService;

    @GetMapping("/games/start")
    public GameStatus startGame(@RequestHeader("token") String tokenId) {
        // Check if player is in game already
        // Check if player has started a game already

        Token token = tokenService.getTokenById(tokenId);
        if (token == null) return null;

        Game game = gameService.createGame(token);
        return game == null ? null : GameStatus.getForPrimary(token, game);
    }

    @GetMapping("/games/join/{gameId}")
    public GameStatus joinGame(@RequestHeader("token") String tokenId, @PathVariable("gameId") String gameId) {
        // Check if player is in game already
        // Check if player has started a game already
        //System.out.println(gameId);


        Token token = tokenService.getTokenById(tokenId);
        if (token == null) return null;


        Game game = gameService.getGame(gameId);
        if (game == null || game.getStatus() != GameStatus.Status.OPEN) return null;


        gameService.joinGame(token, game);
        return GameStatus.get(token, game);
    }

    @GetMapping("/games/status")
    public GameStatus gameStatus(@RequestHeader("token") String tokenId) {
        Token token = tokenService.getTokenById(tokenId);
        if (token == null) return null;

        Game game = token.getGame();
        if (game == null) return null;

        return GameStatus.get(token, game);
    }

    @GetMapping("/games")
    public Collection<GameStatus> gameList(@RequestHeader("token") String tokenId) {
        Token token = tokenService.getTokenById(tokenId);
        if (token == null) return null;

        List<GameStatus> gameStatuses = new ArrayList<>();
        for (Game game : gameService.getGames()) {
            if (game.getStatus() != GameStatus.Status.OPEN) continue;
            gameStatuses.add(GameStatus.getNoContext(game));
        }

        return gameStatuses;

//        return gameService.getGames()
//                .stream()
//                .filter((game) -> game.getStatus() == GameStatus.Status.OPEN)
//                .map(GameStatus::getNoContext).collect(Collectors.toList());
    }

    @GetMapping("/games/{gameId}")
    public GameStatus gameInfo(@RequestHeader("token") String tokenId, @PathVariable("gameId") String gameId) {
        Token token = tokenService.getTokenById(tokenId);
        if (token == null) return null;

        Game game = gameService.getGame(gameId);
        if (game == null) return null;

        return gameId.equals(token.getGame() == null ? null : token.getGame().getId()) ?
                GameStatus.get(token, game) :
                GameStatus.getNoContext(game);
    }

    @GetMapping("/games/move/{sign}")
    public GameStatus makeMove(@RequestHeader("token") String tokenId, @PathVariable("sign") String sign) {
        Token token = tokenService.getTokenById(tokenId);
        if (token == null) return null;

        Game game = token.getGame();
        if (game == null) return null;

        try {
           GameMove move = GameMove.valueOf(sign.toUpperCase());
           gameService.makeMove(token, game, move);
        } catch (IllegalArgumentException ignored) {
            return null;
        }

        return GameStatus.get(token, game);
    }
}
