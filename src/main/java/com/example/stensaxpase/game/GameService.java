package com.example.stensaxpase.game;

import com.example.stensaxpase.tokens.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GameService {

    @Autowired
    GameRepository repository;

    public Game createGame(Token token) {
        Game game = new Game();

        token.setGame(game);
        token.setOpponent(1);
        game.setPlayer(0, token);

        repository.save(game);
        return game;
    }

    public void joinGame(Token token, Game game) {
        token.setOpponent(0);
        token.setGame(game);
        game.setPlayer(1, token);
        game.setStatus(GameStatus.Status.ACTIVE);

        repository.save(game);
    }

    public void makeMove(Token token, Game game, GameMove move) {
        int playerIndex = token.getOpponent() == 0 ? 1 : 0;

        game.setMove(playerIndex, move);
        GameMove opponentMove = game.getMove(token.getOpponent());
        if (opponentMove == GameMove.NONE) {
            repository.save(game);
            return;
        }

        // Kolla vem som vann
        if (move == opponentMove) {
            game.setStatus(GameStatus.Status.DRAW);
            repository.save(game);
            return;
        }

        if ((move == GameMove.ROCK && opponentMove == GameMove.SCISSORS) ||
            (move == GameMove.PAPER && opponentMove == GameMove.ROCK) ||
            (move == GameMove.SCISSORS && opponentMove == GameMove.PAPER)) {
            setWinStatus(game, playerIndex);
        } else {
            setLoseStatus(game, playerIndex);
        }

        repository.save(game);
    }

    public void setWinStatus(Game game, int playerIndex) {
//        if (playerIndex == 0) {
//            game.setStatus(WIN);
//        } else {
//            game.setStatus(LOSE);
//        }

        game.setStatus(playerIndex == 0 ? GameStatus.Status.WIN : GameStatus.Status.LOSE);
    }

    public void setLoseStatus(Game game, int playerIndex) {
        game.setStatus(playerIndex == 0 ? GameStatus.Status.LOSE : GameStatus.Status.WIN);
    }

    public Game getGame(String id) {
        return repository.findById(id).orElse(null);
    }

    public Iterable<Game> getGames() {
        return repository.findAll();
    }
}
