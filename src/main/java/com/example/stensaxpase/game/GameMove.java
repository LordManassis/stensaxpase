package com.example.stensaxpase.game;

public enum GameMove {
    NONE,
    ROCK,
    PAPER,
    SCISSORS
}
