package com.example.stensaxpase.game;

import com.example.stensaxpase.tokens.Token;
import lombok.Getter;

@Getter
public class GameStatus {

    private String gameId;
    private String status;

    private String playerName, playerMove;
    private String opponentName, opponentMove;

    public static GameStatus get(Token token, Game game) {
        GameStatus status = new GameStatus();
        status.gameId = game.getId();
        status.status = game.getStatus().name();
        if (token.getOpponent() == 0){
            if (game.getStatus() == Status.WIN) status.status = Status.LOSE.name();
            if (game.getStatus() == Status.LOSE) status.status = Status.WIN.name();

        }

        status.playerName = token.getName();

        GameMove move = game.getMove(token.getOpponent() == 0 ? 1 : 0);
        status.playerMove = move != null ? move.name() : null;

        Token otherPlayer = game.getPlayer(token.getOpponent());
        status.opponentName = otherPlayer != null ? otherPlayer.getName() : null;

        move = game.getMove(token.getOpponent());
        status.opponentMove = move != null ? move.name() : null;
        return status;
    }

    public static GameStatus getForPrimary(Token token, Game game) {
        GameStatus status = new GameStatus();
        status.gameId = game.getId();
        status.status = game.getStatus().name();
        status.playerName = token.getName();

        return status;
    }

    public static GameStatus getNoContext(Game game) {
        GameStatus status = new GameStatus();
        status.gameId = game.getId();
        status.status = game.getStatus().name();
        status.playerName = game.getPlayer(0).getName();
        return status;
    }

    public enum Status {
        NONE,
        OPEN,
        ACTIVE,
        WIN,
        LOSE,
        DRAW
    }
}
