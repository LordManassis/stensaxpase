package com.example.stensaxpase.game;

import com.example.stensaxpase.tokens.Token;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "games")
public class Game {

    @Getter
    @Id
    private final String id;

    @Getter
    @Setter
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private GameStatus.Status status;

    @OneToOne(cascade = CascadeType.ALL)
    private Token player1, player2;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private GameMove player1Move, player2Move;

    public Game() {
        this.id = UUID.randomUUID().toString();
        this.status = GameStatus.Status.OPEN;
        this.player1Move = GameMove.NONE;
        this.player2Move = GameMove.NONE;
    }

    public GameMove getMove(int player) {
        return player == 0 ? player1Move : player2Move;
    }

    public Token getPlayer(int player) {
        return player == 0 ? player1 : player2;
    }

    public void setMove(int player, GameMove move) {
        GameMove ignored = player == 0 ? (player1Move = move) : (player2Move = move);

//        if (player == 0) player1Move = move;
//        else player2Move = move;

//        if (player == 0) {
//            player1Move = move;
//        } else {
//            player2Move = move;
//        }
    }

    public void setPlayer(int player, Token token) {
        Token ignored = player == 0 ? (player1 = token) : (player2 = token);
    }
}

