package com.example.stensaxpase.tokens;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TokenService {

    @Autowired
    TokenRepository repository;

    public Token createToken() {
        return repository.save(Token.create());
    }

    public void setName(String tokenId, String name) {
        Token token = getTokenById(tokenId);
        if (token == null) return;

        token.setName(name);
        repository.save(token);
    }

    public Token getTokenById(String id) {
        return repository.findById(id).orElse(null);
    }
}
