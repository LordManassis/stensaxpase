package com.example.stensaxpase.tokens;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class TokenController {

    TokenService tokenService;

    @GetMapping("/auth/token")
    public String createToken() {
        return tokenService.createToken().getId();
    }

    @PostMapping("/user/name")
    @ResponseStatus(HttpStatus.OK)
    public void setName(@RequestHeader("token") String tokenId, @RequestBody TokenName name) {
       tokenService.setName(tokenId, name.name);
    }

    private static final class TokenName {
        public String name;
    }
}
