package com.example.stensaxpase.tokens;

import com.example.stensaxpase.game.Game;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Data
@NoArgsConstructor
@Entity
@Table(name = "tokens")
public class Token {

    @Id
    private String id;

    @ManyToOne
    private Game game;
    private String name;
    private int opponent;

    private Token(UUID id) {
        this.id = id.toString();
    }

    public static Token create() {
        return new Token(UUID.randomUUID());
    }
}
